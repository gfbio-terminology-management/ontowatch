package org.gfbio.terminologies.main;

import org.gfbio.terminologies.svc.WatcherSvc;

public class OntoWatch {

  public static void main(String... args) {
    WatcherSvc watchSvc = new WatcherSvc(args);
    watchSvc.startSvc();
  }
}
