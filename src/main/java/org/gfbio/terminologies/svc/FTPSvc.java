package org.gfbio.terminologies.svc;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

/**
 * 
 * @apiNote Service class for downloading files from an FTP server
 *
 */
public class FTPSvc implements OntoService {

  private static final Logger LOGGER = Logger.getLogger(FTPSvc.class);

  private String ontoName;

  private static FTPSvc instance;

  private DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.systemDefault());

  public static FTPSvc getInstance() {
    if (FTPSvc.instance == null) {
      FTPSvc.instance = new FTPSvc();
    }
    return FTPSvc.instance;
  }

  @Override
  public void setOntoName(String ontoName) {
    this.ontoName = ontoName;
  }

  @Override
  public int download(String repo, String targetDir) {

    try {

      FTPClient client = new FTPClient();
      System.out.println(new URL(repo).getHost());
      client.connect(new URL(repo).getHost());
      client.enterLocalPassiveMode();
      client.setFileType(FTP.BINARY_FILE_TYPE);

      OutputStream outputStream1 = new BufferedOutputStream(
          new FileOutputStream(new File(targetDir + File.separator + ontoName + ".owl")));
      boolean success = client.retrieveFile(repo, outputStream1);
      outputStream1.close();

      if (success)
        LOGGER.info("File has been downloaded successfully");

      success = client.completePendingCommand();

      if (success)
        LOGGER.info("all done");

      client.disconnect();

    } catch (IOException e) {
      LOGGER.error("download failed due to " + e.getLocalizedMessage());
    }

    return 0;
  }

  @Override
  public int extract(String file, String targetDir, String onto) {
    throw new NotImplementedException("method not implemented for FTP services");
  }

  @Override
  public LocalDate getLatestRemoteVersion(String repo) {

    String latestRelease = null;
    FTPClient client = new FTPClient();

    try {

      client.connect(new URL(repo).getHost());
      client.changeWorkingDirectory("pub/databases/chebi/ontology/");
      // client.enterLocalPassiveMode();
      FTPFile[] serverFiles = client.mlistDir(repo);
      for (FTPFile ftpFile : serverFiles) {
        LOGGER.info(ftpFile);
      }

      if (client.hasFeature("MDTM") == true) {
        latestRelease = client.getModificationTime(repo + ontoName + ".owl");
        LOGGER.info(latestRelease);
      } else
        LOGGER.info("server does not support MDTM command");

      client.disconnect();
    } catch (IOException e) {
      LOGGER.error("retrieving latest remote version failed due to " + e.getLocalizedMessage());
      e.printStackTrace();
    }

    if (latestRelease != null)
      return LocalDate.parse(latestRelease, formatter);
    else
      return null;
  }

  @Override
  public void cleanUp(String targetDir) {
    throw new NotImplementedException("method not implemented for FTP services");
  }


}
