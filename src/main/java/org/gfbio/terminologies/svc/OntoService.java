package org.gfbio.terminologies.svc;

import java.time.LocalDate;

public interface OntoService {
  public int download(String repo, String targetDir);

  /**
   * 
   * @param file file to unzip
   * @param targetDir folder in which to extract
   * @param onto name of ontology owl file to extract
   * @return return code
   */
  public int extract(String file, String targetDir, String onto);

  public LocalDate getLatestRemoteVersion(String repo);

  public void setOntoName(String ontoName);

  public void cleanUp(String targetDir);
}
