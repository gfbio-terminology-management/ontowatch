package org.gfbio.terminologies.svc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.kohsuke.github.GHAsset;
import org.kohsuke.github.GHCommit;
import org.kohsuke.github.GHCommitQueryBuilder;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;
import org.kohsuke.github.PagedIterable;
import org.kohsuke.github.PagedIterator;

public class GitHubSvc implements OntoService {

  private static GitHubSvc instance;

  private static final Logger LOGGER = Logger.getLogger(GitHubSvc.class);

  private GitHub gh = null;

  private DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.systemDefault());

  private String ontoName;

  public static GitHubSvc getInstance() {
    if (GitHubSvc.instance == null) {
      GitHubSvc.instance = new GitHubSvc();
    }
    return GitHubSvc.instance;
  }

  private GitHubSvc() {

    try {
      connect();
    } catch (IOException e) {
      LOGGER.error("Could not connect to GitHub due to " + e.getLocalizedMessage());
    }
  }

  private void connect() throws IOException {

    LOGGER.info("connecting to GitHub");

    // FIXME remove personal OAuth token
    gh = GitHub.connectUsingOAuth("406a3e4c19e249700d99aa98ec2eca6267a528e9");
    gh.refreshCache();

    LOGGER.info("... done");
  }

  public LocalDate getLatestRemoteVersion(String repository) {

    LOGGER.info("checking repository " + repository);

    String latestRelease = "";

    try {
      latestRelease = formatter
          .format(gh.getRepository(repository).getLatestRelease().getPublished_at().toInstant());
    } catch (IOException e) {
      LOGGER.error("Query failed due to " + e.getLocalizedMessage());
    } catch (NullPointerException npe) {
      try {
        // there is no release available, so we have to search for assets manually
        LOGGER.info(
            "there is no (latest) release available - searching for assets directly in the commit history");

        GHCommitQueryBuilder queryBuilder = gh.getRepository(repository).queryCommits();
        PagedIterable<GHCommit> commits = queryBuilder.list();
        Iterator<GHCommit> iterator = commits.iterator();
        boolean found = false;
        while (found == false) {
          GHCommit commit = iterator.next();
          for (org.kohsuke.github.GHCommit.File f : commit.getFiles()) {
            if (f.getFileName().contains(ontoName + ".owl")) {
              found = true;
              break;
            }
          }
          latestRelease = formatter.format(commit.getCommitShortInfo().getCommitDate().toInstant());

        }
      } catch (IOException e) {
        LOGGER.error("querying commits failed due to " + e.getLocalizedMessage());
      }
    }
    if (latestRelease != null)
      LOGGER.info("found release from " + latestRelease);
    else {
      LOGGER.info(
          "no release could be found, but a file is available. \n setting latest release date to Today");
      latestRelease = formatter.format(LocalDate.now());
    }

    return LocalDate.parse(latestRelease, formatter);
  }

  @Override
  public int download(String repo, String targetDir) {

    String release = null, assetname = null, filename = null, commitDate = null;
    boolean isZip = false;
    GHRepository repository = null;

    try {
      repository = gh.getRepository(repo);

      // release do not need to be zipped, so check for any assets first
      PagedIterator<GHAsset> it = repository.getLatestRelease().listAssets().iterator();
      if (it.hasNext() == true) {
        while (it.hasNext()) {
          GHAsset ass = it.next();
          LOGGER.info(ass.getBrowserDownloadUrl());
          LOGGER.info(ass.getName());

          if (ass.getName().equalsIgnoreCase(ontoName + ".owl")) {
            release = ass.getBrowserDownloadUrl();
            assetname = ass.getName();
          }
        }
        LOGGER.info("found asset release " + release);
      } else {
        release = gh.getRepository(repo).getLatestRelease().getZipballUrl();
        LOGGER.info("zip release found: " + release);
        isZip = true;
      }
    } catch (IOException e) {
      e.printStackTrace();
      LOGGER.error("Getting URL failed due to " + e.getLocalizedMessage());

      return -1;
    } catch (NullPointerException e) {
      try {

        // there is no release available, so we have to search for assets manually
        LOGGER.info(
            "there is no (latest) release available - searching for assets directly in the commit history");

        GHCommitQueryBuilder queryBuilder = repository.queryCommits();
        PagedIterable<GHCommit> commits = queryBuilder.list();
        Iterator<GHCommit> iterator = commits.iterator();
        boolean found = false;
        while (found == false) {
          GHCommit commit = iterator.next();
          for (org.kohsuke.github.GHCommit.File f : commit.getFiles()) {
            if (f.getFileName().contains(ontoName + ".owl")) {
              found = true;
              release = f.getRawUrl().toString();
              assetname = ontoName + ".owl";
              break;
            }
          }
          commitDate = formatter.format(commit.getCommitShortInfo().getCommitDate().toInstant());
          LOGGER.info("found release " + release + " from " + commitDate);
        }

        // Iterator<GHContent> it =
        // gh.getRepository(repo).getDirectoryContent("ontology").iterator();
        // while (it.hasNext()) {
        // GHContent ass = it.next();
        // if (ass.getName().equalsIgnoreCase(ontoName + ".owl")) {
        // release = ass.getDownloadUrl();
        // assetname = ass.getName();
        // LOGGER.info("found file " + assetname + " under URL " + release);
        //
        // commitDate = getDateOfCommit(repository, ass.getSha());
        // }
        // }
        // add found commit date to the target directory path
        // targetDir += commitDate;
      } catch (IOException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }
    }
    try {

      LOGGER.info("starting download");
      LOGGER.info("downloading to " + targetDir);

      Path path = Paths.get(targetDir);
      if (path.toFile().exists() == false)
        Files.createDirectories(path);

      ReadableByteChannel readableByteChannel = Channels.newChannel(new URL(release).openStream());
      if (isZip == true)
        filename = targetDir + File.separator + "test.zip";
      else
        filename = targetDir + File.separator + assetname;

      LOGGER.info("writing to " + filename);
      FileOutputStream fileOutputStream = new FileOutputStream(filename);
      FileChannel fileChannel = fileOutputStream.getChannel();
      fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

      fileOutputStream.close();
      fileChannel.close();


    } catch (IOException e) {
      e.printStackTrace();
      LOGGER.error("download failed due to " + e.getLocalizedMessage());
      return -1;
    }

    LOGGER.info("download complete");

    return 0;

  }

  @Override
  public int extract(String file, String targetDir, String onto) {

    LOGGER.info("extracting file " + file + " to dir " + targetDir);
    try {

      File destDir = new File(targetDir);
      byte[] buffer = new byte[1024];
      try (ZipInputStream zis = new ZipInputStream(new FileInputStream(file))) {

        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
          File newFile = newFile(destDir, zipEntry);
          if (zipEntry.isDirectory()) {
            if (!newFile.isDirectory() && !newFile.mkdirs()) {
              throw new IOException("Failed to create directory " + newFile);
            }
          } else {
            // fix for Windows-created archives
            File parent = newFile.getParentFile();
            if (!parent.isDirectory() && !parent.mkdirs()) {
              throw new IOException("Failed to create directory " + parent);
            }

            // write file content
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
              fos.write(buffer, 0, len);
            }
            fos.close();
          }
          zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        // zis.close();

        LOGGER.info("extract complete");
        LOGGER.info("removing zip file");
        FileUtils.forceDelete(new File(file));
      }
    } catch (FileNotFoundException e) {
      LOGGER.error("file not found");

      return -1;

    } catch (IOException e) {
      LOGGER.error("exception during extract " + e.getLocalizedMessage());
    }

    LOGGER.info("searching for ontology file");
    searchAndCopyFile(targetDir, Paths.get(targetDir).getParent().toString(), onto + ".owl");

    return 0;
  }

  public void cleanUp(String targetDir) {
    try {

      // Clean up afterwards
      FileUtils.deleteDirectory(new File(targetDir));
      LOGGER.info("clean up complete");
    } catch (

    IOException e) {
      LOGGER.error("directory could not be deleted due to " + e.getLocalizedMessage());
    }
  }

  private int searchAndCopyFile(String dir, String target, String fileName) {

    LOGGER.info("copy " + fileName + " from " + dir + " to " + target);

    try (Stream<Path> walkStream = Files.walk(Paths.get(dir))) {
      walkStream.filter(p -> p.toFile().isFile()).forEach(f -> {
        if (f.toString().endsWith(fileName)) {
          System.out.println(f + " found!");
          try {
            FileUtils.copyFile(f.toFile(), new File(target + File.separator + fileName));
          } catch (IOException e) {
            LOGGER.error("copy failed due to " + e.getLocalizedMessage());
          }
        }
      });
    } catch (IOException e) {
      LOGGER.error("file could not be found due to " + e.getLocalizedMessage());
      return -1;
    }
    return 0;
  }

  private File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
    File destFile = new File(destinationDir, zipEntry.getName());

    String destDirPath = destinationDir.getCanonicalPath();
    String destFilePath = destFile.getCanonicalPath();

    if (!destFilePath.startsWith(destDirPath + File.separator)) {
      throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
    }

    return destFile;
  }

  @Override
  public void setOntoName(String name) {
    this.ontoName = name;
  }
}
