package org.gfbio.terminologies.svc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.log4j.Logger;
import org.gfbio.config.ConfigSvc;

/**
 * Class for wrapping process calls or calls to external programs, i.e., jars and whatnot
 *
 */
public class ProcessSvc {

  private static ProcessSvc instance;

  private static final Logger LOGGER = Logger.getLogger(ProcessSvc.class);

  private ConfigSvc cfgSvc;

  enum Mode {
    DIFF, UPDATE
  }

  public static ProcessSvc getInstance() {
    if (ProcessSvc.instance == null) {
      ProcessSvc.instance = new ProcessSvc();
    }
    return ProcessSvc.instance;
  }


  private ProcessSvc() {
    cfgSvc = ConfigSvc.getInstance();
  }

  /**
   * 
   * @param mode what external program to start
   * @param opts optional parameters
   */
  public int start(Mode mode, String... opts) {

    int rc = 0;
    List<String> command = new ArrayList<String>();

    command.add("java");
    // Specifies a list of directories, JAR files, and ZIP archives to search for class files
    command.add("-jar");
    command.add("-Xmx" + cfgSvc.getSettingFromINI("GENERAL", "jvmXmx"));
    command.add("-Xms" + cfgSvc.getSettingFromINI("GENERAL", "jvmXms"));

    switch (mode) {
      case DIFF:
        command.add((String) cfgSvc.getSettingFromINI("GENERAL", "workDir")
            + cfgSvc.getSettingFromDB("general", "name", mode.name(), "jarName"));
        command.addAll(Arrays.asList(opts));
        break;

      case UPDATE:
        command.add((String) cfgSvc.getSettingFromINI("GENERAL", "workDir")
            + cfgSvc.getSettingFromDB("general", "name", mode.name(), "jarName"));
        command.addAll(Arrays.asList(opts));
        break;

      default:
        // TODO
        break;
    }

    LOGGER.info("start command " + command);

    ProcessBuilder pb = new ProcessBuilder(command);
    pb.inheritIO();

    try {
      Process p = pb.start();
      rc = p.waitFor();
    } catch (IOException e) {
      LOGGER.error("start failed due to " + e.getLocalizedMessage());
    } catch (InterruptedException e) {
      LOGGER.error("waitFor failed due to " + e.getLocalizedMessage());
    }

    return rc;
  }
}
