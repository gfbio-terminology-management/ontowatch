package org.gfbio.terminologies.svc;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.log4j.Logger;

/**
 * 
 * @apiNote Service class for directly downloading files from an online source
 *
 */
public class DirectSvc implements OntoService {

  private static final Logger LOGGER = Logger.getLogger(DirectSvc.class);

  private String ontoName;

  private static DirectSvc instance;

  private DateTimeFormatter formatter =
      DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.systemDefault());

  public static DirectSvc getInstance() {
    if (DirectSvc.instance == null) {
      DirectSvc.instance = new DirectSvc();
    }
    return DirectSvc.instance;
  }

  private DirectSvc() {

  }

  private static final int CONNECT_TIMEOUT = 10000, READ_TIMEOUT = 10000;

  @Override
  public int download(String repo, String targetDir) {

    LOGGER.info(
        "downloading file from " + repo + " to " + targetDir + File.separator + ontoName + ".owl");

    try {
      FileUtils.copyURLToFile(new URL(repo + ontoName + ".owl"),
          new File(targetDir + File.separator + ontoName + ".owl"), CONNECT_TIMEOUT, READ_TIMEOUT);
    } catch (IOException e) {
      LOGGER.error("download failed due to " + e.getLocalizedMessage());
      return -1;
    }

    LOGGER.info("download completed successfully");

    return 0;
  }

  @Override
  public int extract(String file, String targetDir, String onto) {
    throw new NotImplementedException("method not implemented for Direct services");
  }

  @Override
  public LocalDate getLatestRemoteVersion(String repo) {

    // try {
    // FileTime creationTime =
    // (FileTime) Files.getAttribute(Paths.get(repo + ontoName + ".owl"), "creationTime");
    //
    // LOGGER.info(creationTime);
    // } catch (IOException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }

    LOGGER.info(
        "no release could be found, but a file is available. \n setting latest release date to Today");
    String latestRelease = formatter.format(LocalDate.now());


    return LocalDate.parse(latestRelease, formatter);
  }

  @Override
  public void setOntoName(String ontoName) {
    this.ontoName = ontoName;
  }

  @Override
  public void cleanUp(String targetDir) {
    throw new NotImplementedException("method not implemented for Direct services");
  }

}
