package org.gfbio.terminologies.svc;

import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.gfbio.config.ConfigSvc;
import org.gfbio.db.VirtGraphSingleton;
import org.gfbio.terminologies.svc.ProcessSvc.Mode;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

public class WatcherSvc {

  static final Logger LOGGER = Logger.getLogger(WatcherSvc.class);

  private final String uriPrefix = "http://terminologies.gfbio.org/terms/";

  private int rate, delay;

  private ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

  OntoService ontoSvc;

  ProcessSvc pSvc;

  ConfigSvc cfgSvc;

  private String[] args;

  public WatcherSvc(String[] args) {
    this.args = args;

    // TODO init repository services
    pSvc = ProcessSvc.getInstance();
    cfgSvc = ConfigSvc.getInstance();
    setup();
  }

  public void startSvc() {

    LOGGER.info("starting WatcherService");

    LOGGER.info("rate=" + rate + "min; delay=" + delay + "min");

    List<String> ontologies = listInternalOntologies();

    LOGGER.info("watching " + ontologies.toString());

    // executes task every 10 seconds, after 1 sec delay
    scheduler.scheduleAtFixedRate(new Runnable() {

      // do for every internal ontology if possible
      // (1) check releases
      // (2) if newer release is available download and extract
      // (3) get OWL file and copy to diff folder and calculate version diff

      @Override
      public void run() {

        for (Object obj : ontologies) {

          String acronym = (String) obj;
          // read property 'location'
          String repo =
              (String) cfgSvc.getSettingFromDB("ontologies", "acronym", acronym, "location");

          if (repo.equalsIgnoreCase("???")) {
            LOGGER.info("no valid location available for " + acronym + "; skipping");
            continue;
          }

          // read property 'name'
          // it also acts as the proper filename, since there can be different acronyms and ontology
          // names, e.g., for PTO
          String ontoFileName =
              (String) cfgSvc.getSettingFromDB("ontologies", "acronym", acronym, "name");
          LOGGER.info("checking " + repo + " for ontology " + ontoFileName);

          if (repo.contains("ftp://"))
            // ontoSvc = FTPSvc.getInstance();
            ontoSvc = DirectSvc.getInstance();
          else
            ontoSvc = GitHubSvc.getInstance();

          ontoSvc.setOntoName(ontoFileName);

          LocalDate current = getLatestInternalRelease(acronym);
          LocalDate latest = ontoSvc.getLatestRemoteVersion(repo);

          LOGGER.info("latest release: " + latest);
          LOGGER.info("current release: " + current);

          if (current == null || latest.isAfter(current)) {
            LOGGER.info("Newer version available: " + latest);

            String targetDir =
                cfgSvc.getOntoDir() + acronym.toLowerCase() + File.separator + latest;

            int rc = ontoSvc.download(repo, targetDir);
            if (rc == 0) {
              if (new File(targetDir + File.separator + "test.zip").exists() == true) {
                // extract files from archive
                rc = ontoSvc.extract(targetDir + File.separator + "test.zip",
                    targetDir + File.separator + "unzip", ontoFileName);

                // clean up afterwards
                ontoSvc.cleanUp(targetDir + File.separator + "unzip");
              }
            }
            // check file size first: DIFF/UPDATE may not work for very large ontologies, thus, we
            // skip
            // them!
            long sizeInBytes = FileUtils.sizeOf(new File(cfgSvc.getOntoDir() + acronym.toLowerCase()
                + File.separator + latest + File.separator + ontoFileName + ".owl"));
            double sizeInMiB = (double) sizeInBytes / FileUtils.ONE_MB;
            if (rc == 0 && current != null) {
              // now the latest version can be used for comparison
              // arguments: ontology name (for loading the right property file), path to ontology1,
              // path to ontology2

              if (sizeInMiB < 1000) {
                rc = pSvc.start(Mode.DIFF,
                    new String[] {acronym,
                        cfgSvc.getOntoDir() + acronym.toLowerCase() + File.separator + current
                            + File.separator + ontoFileName + ".owl",
                        cfgSvc.getOntoDir() + acronym.toLowerCase() + File.separator + latest
                            + File.separator + ontoFileName + ".owl"});
              } else {
                LOGGER.info("Size of ontology exceeded the maximum (" + sizeInMiB + " > 1000)");
                LOGGER.info("DIFF was not calculated. This needs to be done manually!");
              }
            }
            if (rc == 0) {
              if (sizeInMiB < 1000) {
                LOGGER.info("all done --- diff is now available");
                LOGGER.info("updating ontology version now");
                // arguments: ontology name, yyyy-mm-dd of latest version (will point to actual
                // folder
                // in filesystem)
                rc = pSvc.start(Mode.UPDATE, new String[] {acronym, latest.toString()});

                if (rc == 0)
                  LOGGER.info("UPDATE done");
              } else {
                LOGGER.info("Size of ontology exceeded the maximum (" + sizeInMiB + " > 1000)");
                LOGGER.info("DIFF was not calculated. This needs to be done manually!");
              }
            }

          }
        }
      }
    }, delay, rate, TimeUnit.MINUTES);
  }

  public void stopSvc() {
    scheduler.shutdown();
  }

  /**
   * read configuration parameters and setup
   */
  private void setup() {
    rate = Integer.valueOf(
        cfgSvc.getSettingFromDB("general", "name", "ONTOWATCH", "schedule_rate").toString());
    delay = Integer.valueOf(
        cfgSvc.getSettingFromDB("general", "name", "ONTOWATCH", "schedule_delay").toString());
  }

  private List<String> listInternalOntologies() {

    if (args.length > 0)
      return Arrays.asList(this.args);

    LOGGER.info("listing internal ontologies");

    List<String> ontologies = new ArrayList<String>();

    StringBuilder query = new StringBuilder("SELECT ?acronym ?uri ?name ?description " + "FROM <"
        + uriPrefix + "Metadata" + "> " + "WHERE {?s a omv:Ontology . ?s omv:acronym ?acronym . "
        + "?s gfbio:graph ?graph . ?s omv:URI ?uri . ?s omv:name ?name . ?s omv:description ?description}");
    LOGGER.info(query);
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query.toString(), set);

    try {
      ResultSet rs = vqe.execSelect();

      while (rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        ontologies.add(currentResult.get("acronym").asLiteral().toString().replace(uriPrefix, ""));
      }
    } catch (Exception e) {
      LOGGER.info(e);
    }
    if (ontologies.size() == 0) {
      LOGGER.info("no ontologies were found");
      LOGGER.info("reading pre-defined parameters from " + uriPrefix + "Parameters");
      query = new StringBuilder("SELECT ?o FROM <" + uriPrefix + "Parameters" + "> "
          + "WHERE {?s ?p ?o. FILTER regex(?p, \"acronym\", \"i\")}");
      LOGGER.info(query);
      set = VirtGraphSingleton.getInstance().getVirtGraph();
      vqe = VirtuosoQueryExecutionFactory.create(query.toString(), set);

      try {
        ResultSet rs = vqe.execSelect();

        while (rs.hasNext()) {
          QuerySolution currentResult = rs.next();
          ontologies.add(currentResult.get("o").asLiteral().toString());
        }
      } catch (Exception e) {
        LOGGER.info(e.getLocalizedMessage());
      }

    }

    // FIXME FB 01/26/20 also read configured ontologies and get the intersection
    // return Arrays.asList(new HashSet(Stream
    // .concat(ontologies.stream(), cfgSvc.getSettingsFromDB("ontologies", "acronym").stream())
    // .collect(Collectors.toList())).toArray());

    return ontologies;
  }


  public LocalDate getLatestInternalRelease(String terminology_id) {

    String latest = null;
    DateTimeFormatter formatter =
        DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneId.systemDefault());;
    // query Virtuoso
    // get metadata for ontology
    // compare internal version and latest available version dates


    String metaDataGraph = "Metadata";

    int hash = terminology_id.hashCode();
    String metadata_query = "SELECT ?o FROM <" + uriPrefix + metaDataGraph + ">" + " WHERE {<"
        + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . <" + uriPrefix + hash
        + "> ?p ?o. FILTER(?p = gfbio:releaseDate || ?p = omv:creationDate)}";
    LOGGER.info(metadata_query);
    VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
    vqe.setTimeout(1);

    try {
      ResultSet rs = vqe.execSelect();

      while (rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        latest = currentResult.get("o").asLiteral().getValue().toString();
      }
      if (latest == null)
        return null;
    } catch (Exception e) {
      e.printStackTrace();
    }

    return LocalDate.parse(latest, formatter);
  }

}
